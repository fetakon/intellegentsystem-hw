#include "MeAuriga.h"
#include "SoftwareSerial.h"

#define STOP            0
#define APP_CONTROLL    1
#define SELF_DRIVING    2
#define FORWARD         3
#define BACKWARD        4
#define LEFT            5
#define RIGHT           6
#define OBSTACLE        7
#define BOARDER         8
#define MANUALSTOP      9

MeEncoderOnBoard Encoder_1(SLOT1); //Motor1 is Right Motor, M1
MeEncoderOnBoard Encoder_2(SLOT2); //Motor2 is Left Motor, M2
MeLineFollower ir(PORT_8);  //Ir, Port 8
MeUltrasonicSensor us(PORT_10); //Ultrasonic, PORT 10

uint8_t irRead = 0;
int16_t moveSpeed = 180;
int16_t distance = 0;
int moverstate = -1;
int previousState = 0;
int manualstate = 0;
int stateselect = 0;
int manualstop = 0;

char inData;
char outData;

bool lineOK = true;
bool forwardFlag = false;
bool backwardFlag = false;
bool rightFlag = false;
bool leftFlag = false;

void printprocess(void);
void Forward(void);
void Backward(void);
void BackwardAndTurnRight(void);
void BackwardAndTurnLeft(void);
void TurnRight(void);
void TurnLeft(void);
void Stop(void);
void irProcess(void);
void usProcess(void);
void appControllProcess(void);
void backwardAndTurnLeft(void);
void backwardAndTurnRight(void);

//Make the robot drive forward
void Forward(void)
{
  Encoder_1.setMotorPwm(-moveSpeed);
  Encoder_2.setMotorPwm(moveSpeed);
}

//Make the robot drive backwards
void Backward(void)
{
  Encoder_1.setMotorPwm(moveSpeed);
  Encoder_2.setMotorPwm(-moveSpeed);
}

//Make the robot turn left
void TurnLeft(void)
{
  Encoder_1.setMotorPwm(-moveSpeed/2);
  Encoder_2.setMotorPwm(0);
}

//Make the robot turn right
void TurnRight(void)
{
  Encoder_1.setMotorPwm(0);
  Encoder_2.setMotorPwm(moveSpeed/2);
}


//Make the mower left on the spot
void backwardAndTurnLeft(){
  Encoder_1.setMotorPwm(moveSpeed/4);
  Encoder_2.setMotorPwm(-moveSpeed);
  delay(1000);
}

//Make the mower right on the spot
void backwardAndTurnRight(){
  Encoder_1.setMotorPwm(moveSpeed);
  Encoder_2.setMotorPwm(-moveSpeed/4);
  delay(1000);
}

//Make the robot stop
void Stop(void)
{
  Encoder_1.setMotorPwm(0);
  Encoder_2.setMotorPwm(0);
}

//The robot is driving forward as long as both IR sensors are active. 
//If non of the IR sensors are active, then the robot will drive backwards for two seconds and then turn left for two seconds.
//If only one of the IR sensors are active, then the robot will turn will turn left or right depending on which sensors that is active.

void irProcess()
{
  int sensorState = ir.readSensors();
  switch(sensorState)
  {
    case S1_OUT_S2_OUT:
      if(previousState != FORWARD)
      {
        previousState = FORWARD;
        Serial.write(FORWARD);
      }
      Forward();
    break;
    case S1_IN_S2_IN:
      if(previousState != BACKWARD)
      {
        Serial.write(BOARDER);
        delay(100);
        Serial.write(LEFT);
        previousState = BACKWARD;
      }
      backwardAndTurnLeft();
    break;
    case S1_IN_S2_OUT:
      if(previousState != RIGHT)
      {
        Serial.write(BOARDER);
        delay(100);
        Serial.write(RIGHT);
        previousState = RIGHT;
      }
      backwardAndTurnRight();
    break;
    case S1_OUT_S2_IN:
      if(previousState != LEFT)
      {
        Serial.write(BOARDER);
        delay(100);
        Serial.write(LEFT);
        previousState = LEFT;
      }
      backwardAndTurnLeft();
    break;
    default:
    break;
  }

  
}

//If the distance is between the ultrasonic sensor and an object is between 20 and 40 centimeters, then the robot will turn right for two seconds.
//A 'r' will be sent serialy to the application, when the robot turns.
//If the distance is not between the interval, then the robot will drive according to irProcess().
void usProcess()
{
  
  distance = us.distanceCm();
  if((distance > 10) && (distance < 30))
  {
    Serial.write(OBSTACLE);
    delay(10);
    Serial.write(BACKWARD);
    Backward();
    delay(500);
    Serial.write(RIGHT);
    TurnRight();
    delay(2500);
  }
  
  else
  {
    irProcess();
  }
}

//the app controlling the robot by sending commands to the robot.
//'3' for FORWARD
//'4' for backwards
//'5' for turn left 
//'6' for turn right

void appControllProcess()
{
  manualstate = Serial.read();

  switch(manualstate)
  {
    case FORWARD:
      Forward();
    break;
    case BACKWARD:
      Backward();
    break;
    case LEFT:
      TurnLeft();
    break;
    case RIGHT:
      TurnRight();
    break;
    case STOP:
      Stop();
    break;
    case MANUALSTOP:
      moverstate = -1;
    break;
  }
}


void setup(){
  
  Serial.begin(115200);
  Serial.println("bluetooth start");
}


//the app is sending a 1 to the robot if the using will controll by app, and a 2 for self-driving mode.
void loop()
{
  
  if(moverstate == -1){
    stateselect = Serial.read();
    
    switch(stateselect)
    {
      case APP_CONTROLL:
        moverstate = APP_CONTROLL;
      break;
      case SELF_DRIVING:
        moverstate = SELF_DRIVING;
      break;
    }
  }
  else
  {
    switch(moverstate)
    {
      case APP_CONTROLL:
        appControllProcess();
      break;
  
      case SELF_DRIVING:
        
        manualstop = Serial.read();
        
        if((manualstop == MANUALSTOP) || (manualstop == APP_CONTROLL)){
          Stop();
          moverstate = -1;
          manualstop = 0;
        }
        else
          usProcess();
      break;
      case STOP:
        Stop();
      break;
    }
  }
  
}
